#!/bin/bash

VERSION=1.0.0

pushd sources
tar cvJf ../deskos-backgrounds-${VERSION}.tar.xz --dereference deskos-backgrounds-${VERSION}
popd
